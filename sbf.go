// Package sbf reads files and data streams in the Septentrio Binary Format.
package sbf

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"io"
	"time"

	"github.com/pkg/errors"
)

const (
	// Length of a GPS week
	weekLen time.Duration = time.Second * 604800
	// Do-Not-use values for TimeStamp fields
	DNUTow  uint32 = 4294967295
	DNUWeek uint16 = 65535
)

var (
	gpsEpoch time.Time = time.Date(1980, time.January, 6, 0, 0, 0, 0, time.UTC)
	// Leap seconds since the GPS epoch
	leapSeconds time.Duration = time.Second * 18
)

// Set the number of leap seconds since the start of the GPS Epoch.
func SetLeapSeconds(count int) {
	leapSeconds = time.Duration(count) * time.Second
}

// SBF block time stamp
type TimeStamp struct {
	// Millisecond of the week
	Tow uint32
	// GPS week number
	Week uint16
}

// IsValid returns true if the TimeStamp is a valid value.
func (ts TimeStamp) IsValid() bool {
	return ts.Tow != DNUTow && ts.Week != DNUWeek
}

// Time converts a TimeStamp to a time.Time value
func (ts TimeStamp) Time() (t time.Time) {
	if ts.IsValid() {
		offset := time.Duration(ts.Tow) * time.Millisecond
		weeks := time.Duration(ts.Week) * weekLen
		t = gpsEpoch.Add(weeks).Add(offset).Add(-leapSeconds)
	}
	return t
}

// MarshalJSON implements the json.Marshaler interface
func (ts TimeStamp) MarshalJSON() ([]byte, error) {
	return json.Marshal(ts.Time())
}

// Zstring is a nul-padded ASCII string
type Zstring struct {
	s []byte
}

func NewZstring(n int) Zstring {
	return Zstring{s: make([]byte, n)}
}

func (z Zstring) MarshalJSON() ([]byte, error) {
	return json.Marshal(string(z.s))
}

func (z *Zstring) UnmarshalJSON(b []byte) error {
	copy(z.s, b)
	return nil
}

type MeasFlags uint8

const (
	MultipathMitigation MeasFlags = (1 << iota)
	Smoothed
	PhaseAlign
	ClockSteering
	// Bit 4: unused
	_
	HighDynamics
	// Bit 6: reserved
	_
	Scrambling
)

type measEpoch struct {
	// Receiver time stamp
	T TimeStamp `json:"t"`
	// Number of MeasEpochChannelType1 sub-blocks
	N1 uint8 `json:"-"`
	// Length of a MeasEpochChannelType1 sub-block
	Sb1Length uint8 `json:"-"`
	// Length of a MeasEpochChannelType2 sub-block
	Sb2Length uint8 `json:"-"`
	// Bitfield of common measurement flags
	CommonFlags MeasFlags `json:"meas_flags"`
	// Cummulative millisecond clock jumps since startup
	CumClkJumps uint8 `json:"clk_jumps"`
	_           uint8
}

const (
	CodeMSBMask uint8  = 0x0f
	DNUSvid     uint8  = 0
	DNUcn0      uint8  = 255
	DNULockTime uint16 = 65535
	DNUDoppler  int32  = -2147483648
)

type measEpochChannelType1 struct {
	// Receiver channel
	RxChannel uint8 `json:"rxchan"`
	// Signal type and antenna ID
	Type uint8 `json:"type"`
	// Satellite ID
	Svid uint8 `json:"svid"`
	// MSB of the pseudo-range in lower bits
	Misc uint8 `json:"misc"`
	// LSB of the pseudo-range
	CodeLSB uint32 `json:"code_lsb"`
	// Carrier doppler in Hz*10^4
	Doppler int32 `json:"doppler"`
	// LSB of the carrier phase relative to the pseudorange
	CarrierLSB uint16 `json:"carrier_lsb"`
	// MSB of the carrier phase relative to the pseudorange
	CarrierMSB int8  `json:"carrier_msb"`
	Cn0        uint8 `json:"cn0"`
	// Duration of continuous carrier phase in seconds
	LockTime uint16 `json:"lock_time"`
	ObsInfo  uint8  `json:"obs_info"`
	// Number of MeasEpochChannelType2 sub-sub-blocks
	N2 uint8 `json:"-"`
}

// RinexId returns the RINEX ID string for an SBF satellite ID.
func RinexId(svid int) (id string) {
	if svid == 0 {
		id = ""
	} else if svid <= 37 {
		id = fmt.Sprintf("G%02d", svid)
	} else if svid <= 61 {
		id = fmt.Sprintf("R%02d", svid-37)
	} else if svid == 62 {
		id = ""
	} else if svid <= 68 {
		id = fmt.Sprintf("R%02d", svid-38)
	} else if svid <= 106 {
		id = fmt.Sprintf("E%02d", svid-70)
	} else if svid <= 119 {
		id = ""
	} else if svid <= 140 {
		id = fmt.Sprintf("S%02d", svid-100)
	} else if svid <= 177 {
		id = fmt.Sprintf("C%02d", svid-140)
	} else if svid <= 187 {
		id = fmt.Sprintf("J%02d", svid-180)
	} else if svid <= 197 {
		id = fmt.Sprintf("I%02d", svid-190)
	} else if svid <= 215 {
		id = fmt.Sprintf("S%02d", svid-157)
	}

	return id
}

// SignalNumber returns the signal number code for this channel.
func (m measEpochChannelType1) SignalNumber() int {
	return int(m.Type) & 0x1f
}

// FreqNr returns the GLONASS frequency number
func (m measEpochChannelType1) FreqNr() int {
	return (int(m.ObsInfo) & 0xf8) >> 3
}

// CodeMSB returns the most-significant byte of the encoded pseudorange
func (m measEpochChannelType1) CodeMSB() uint8 {
	return m.Misc & CodeMSBMask
}

// Snr returns the signal to noise ratio in dB-Hz
func (m measEpochChannelType1) Snr() (snr float64) {
	if m.Cn0 == DNUcn0 {
		return snr
	}

	switch m.SignalNumber() {
	case SigGpsL1P, SigGpsL2P:
		snr = float64(m.Cn0) * 0.25
	default:
		snr = float64(m.Cn0)*0.25 + 10.
	}

	return snr
}

// PseudoRange returns the pseudorange to the satellite in meters
func (m measEpochChannelType1) PseudoRange() float64 {
	return (float64(m.CodeMSB())*4294967296 + float64(m.CodeLSB)) * 0.001
}

// Lambda returns the carrier wavelength in meters
func (m measEpochChannelType1) Lambda() (λ float64) {
	fL := signalTable[m.SignalNumber()].f
	if fL <= 0 {
		return λ
	}

	switch m.SignalNumber() {
	case SigGlonassL1CA, SigGlonassL1P:
		fL = fL + float64(m.FreqNr()-8)*9.0/16.0
	case SigGlonassL2P, SigGlonassL2CA:
		fL = fL + float64(m.FreqNr()-8)*7.0/16.0
	}

	λ = CLIGHT / fL
	return λ
}

// Phase returns the carrier phase in cycles.
func (m measEpochChannelType1) Phase() (phase float64) {
	λ := m.Lambda()
	if λ == 0 {
		return phase
	}

	phase = m.PseudoRange()/λ +
		(float64(m.CarrierMSB)*65536.0+float64(m.CarrierLSB))*0.001
	return phase
}

// DopplerShift returns the doppler shift in Hz.
func (m measEpochChannelType1) DopplerShift() (ds float64) {
	if m.Doppler != DNUDoppler {
		ds = float64(m.Doppler) * 1e-4
	}

	return ds
}

// IsValid returns true if this sub-block is valid
func (m measEpochChannelType1) IsValid() bool {
	return m.Svid != DNUSvid && m.PseudoRange() != 0
}

// MeasEpochChannelType2 sub-sub-block
type MeasEpochChannelType2 struct {
	Type             uint8  `json:"type"`
	LockTime         uint8  `json:"lock_time"`
	Cn0              uint8  `json:"cn0"`
	OffsetsMSB       uint8  `json:"offsets_msb"`
	CarrierMSB       int8   `json:"carrier_msb"`
	ObsInfo          uint8  `json:"obs_info"`
	CodeOffsetLSB    uint16 `json:"code_offset_lsb"`
	CarrierLSB       uint16 `json:"carrier_lsb"`
	DopplerOffsetLSB uint16 `json:"dpl_offset_lsb"`
}

// SignalNumber returns the signal number code for this channel.
func (m MeasEpochChannelType2) SignalNumber() int {
	return int(m.Type) & 0x1f
}

func (m MeasEpochChannelType2) CodeOffsetMSB() (msb int) {
	msb = int(m.OffsetsMSB) & 0x07
	if msb > 3 {
		msb -= 8
	}
	return msb
}

func (m MeasEpochChannelType2) DopplerOffsetMSB() (msb int) {
	msb = (int(m.OffsetsMSB) & 0xf8) >> 3
	if msb > 15 {
		msb -= 32
	}
	return msb
}

// PseudoRange returns the pseudorange to the satellite. M1 is the enclosing
// MeasEpochChannelType1 struct.
func (m MeasEpochChannelType2) PseudoRange(m1 MeasEpochChannelType1) float64 {
	return m1.PseudoRange() + (float64(m.CodeOffsetMSB())*65536+float64(m.CodeOffsetLSB))*0.001
}

// Lambda returns the carrier wavelength in meters. M1 is the enclosing
// MeasEpochChannelType1 struct.
func (m MeasEpochChannelType2) Lambda(m1 MeasEpochChannelType1) (λ float64) {
	fL := signalTable[m.SignalNumber()].f
	if fL <= 0 {
		return λ
	}

	switch m.SignalNumber() {
	case SigGlonassL1CA, SigGlonassL1P:
		fL = fL + float64(m1.FreqNr()-8)*9.0/16.0
	case SigGlonassL2P, SigGlonassL2CA:
		fL = fL + float64(m1.FreqNr()-8)*7.0/16.0
	}

	λ = CLIGHT / fL
	return λ
}

// Phase returns the carrier phase in cycles. M1 is the enclosing
// MeasEpochChannelType1 struct.
func (m MeasEpochChannelType2) Phase(m1 MeasEpochChannelType1) (phase float64) {
	λ := m.Lambda(m1)
	if λ == 0 {
		return phase
	}

	phase = m.PseudoRange(m1)/λ +
		(float64(m.CarrierMSB)*65536.0+float64(m.CarrierLSB))*0.001
	return phase
}

// MeasEpochChannelType1 sub-block
type MeasEpochChannelType1 struct {
	measEpochChannelType1
	Sub []MeasEpochChannelType2 `json:"sub"`
}

// MeasEpoch block definition (ID 4027)
type MeasEpoch struct {
	measEpoch
	Sub []MeasEpochChannelType1 `json:"sub"`
}

// ReadMeasEpoch unpacks a MeasEpoch instance from a ReadSeeker
func ReadMeasEpoch(rdr io.ReadSeeker) (m MeasEpoch, err error) {
	err = binary.Read(rdr, ByteOrder, &m.measEpoch)
	if err != nil {
		return m, err
	}

	padding1 := int(m.Sb1Length) - binary.Size(measEpochChannelType1{})
	padding2 := int(m.Sb2Length) - binary.Size(MeasEpochChannelType2{})

	m.Sub = make([]MeasEpochChannelType1, m.N1)
	for i := 0; i < int(m.N1); i++ {
		err = binary.Read(rdr, ByteOrder, &m.Sub[i].measEpochChannelType1)
		if err != nil {
			return m, errors.Wrapf(err, "decode sub block %d", i)
		}
		// Skip past the padding
		rdr.Seek(int64(padding1), io.SeekCurrent)

		// Read the type 2 sub-sub blocks
		if m.Sub[i].N2 > 0 {
			m.Sub[i].Sub = make([]MeasEpochChannelType2, m.Sub[i].N2)
			for j := 0; j < int(m.Sub[i].N2); j++ {
				err = binary.Read(rdr, ByteOrder, &m.Sub[i].Sub[j])
				if err != nil {
					return m, errors.Wrapf(err, "decode sub-sub block %d,%d", i, j)
				}
				// Skip past the padding
				rdr.Seek(int64(padding2), io.SeekCurrent)
			}
		}
	}

	return m, err
}

// Do-not-use values for PvtGeodetic fields
const (
	DNULat float64 = -2e10
	DNULon float64 = -2e10
	DNUCog float32 = -2e10
)

// PVTGeodetic blocks (ID 4007)
type PvtGeodetic struct {
	// Receiver time stamp
	T TimeStamp `json:"t"`
	// PVT mode
	Mode uint8 `json:"mode"`
	// PVT error code (0 == no error)
	Error uint8 `json:"error"`
	// Latitude in radians
	Lat float64 `json:"lat"`
	// Longitude in radians
	Lon float64 `json:"lon"`
	// Height above ellipsoid in meters
	Height float64 `json:"height"`
	// Geoid undulation in meters
	Undulation float32 `json:"und"`
	// North velocity in m/s
	Vn float32 `json:"vn"`
	// East velocity in m/s
	Ve float32 `json:"ve"`
	// Up velocity in m/s
	Vu float32 `json:"vu"`
	// Course over ground in degrees True, invalid when speed < 0.1 m/s
	Cog float32 `json:"cog"`
	// Receiver clock bias in ms
	RxClkBias float64 `json:"clk_bias"`
	// Receiver clock drift in ppm
	RxClkDrift float32 `json:"clk_drift"`
	// Time system used
	TimeSys uint8 `json:"time_sys"`
	// Coordinate datum
	Datum uint8 `json:"datum"`
	// Number of satellites used in PVT computation
	NrSv uint8 `json:"nr_sv"`
	// Wide-area correction bit field
	WaCorrInfo uint8 `json:"wa_corr"`
	// Reference ID for differential corrections
	RefId uint16 `json:"ref_id"`
	// Mean correction age in 10ms ticks
	MeanCorrAge uint16 `json:"mean_corr_age"`
	// Signal info bit field
	SignalInfo uint32 `json:"sig_info"`
	// Alert flag bit field
	AlertFlag uint8 `json:"alerts"`
}

type PvtGeodeticRev1 struct {
	PvtGeodetic
	// Number of base stations used in PVT computation
	NrBases uint8 `json:"nr_base"`
	// PPP bit field
	PppInfo uint16 `json:"ppp_info"`
}

type PvtGeodeticRev2 struct {
	PvtGeodeticRev1
	// PVT latency in 100us ticks
	Latency uint16 `json:"latency"`
	// Horizontal accuracy in cm
	HAccuracy uint16 `json:"hacc"`
	// Vertical accuracy in cm
	VAccuracy uint16 `json:"vacc"`
	// Miscellaneous flags bit field
	Misc uint8 `json:"misc"`
}

const (
	// Do-not-use value for DOP.*dop fields
	DNUDop uint16 = 0
	// Do-not-use value for protection level fields
	DNUpl float32 = -2e10
	// Scale factor for DOP.*dop fields
	DOP_SCALE float32 = 100
)

// DOP is the Dilution of Precision block (ID 4001)
type DOP struct {
	// Receiver time stamp
	T TimeStamp `json:"t"`
	// Number of satellites used in DOP computation
	NrSv uint8 `json:"nr_sv"`
	_    uint8
	// Position DOP * 0.01
	Pdop uint16 `json:"pdop"`
	// Time DOP * 0.01
	Tdop uint16 `json:"tdop"`
	// Horizontal DOP * 0.01
	Hdop uint16 `json:"hdop"`
	// Vertical DOP * 0.01
	Vdop uint16 `json:"vdop"`
	// Horizontal protection level in meters
	Hpl float32 `json:"hpl"`
	// Vertical protection level in meters
	Vpl float32 `json:"vpl"`
}

// SatVisibility block (ID 4012)
type SatVisibility struct {
	// Receiver time stamp
	T TimeStamp `json:"t"`
	// Number of SatInfo sub-blocks
	N uint8 `json:"-"`
	// Length of a SatInfo sub-block
	SbLength uint8 `json:"-"`
	Sub      []SatInfo
}

type RiseSetStatus uint8

const (
	RsSetting RiseSetStatus = 0
	RsRising                = 1
	RsUnknown               = 255
)

type SatInfoSource uint8

const (
	SiAlmanac   SatInfoSource = 1
	SiEphemeris               = 2
	SiUnknown                 = 255
)

// SatInfo sub-block
type SatInfo struct {
	// Satellite ID
	Svid uint8 `json:"svid"`
	// GLONASS frequency number
	FreqNr uint8 `json:"freqnr"`
	// Azimuth in 0.01 degree units
	Azimuth uint16 `json:"az"`
	// Elevation in 0.01 degree uints
	Elevation int16 `json:"elev"`
	// Rise/set status
	RiseSet RiseSetStatus `json:"rise_set"`
	// Info source
	InfoSrc SatInfoSource `json:"info_src"`
}
