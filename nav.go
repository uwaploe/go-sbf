package sbf

// GPSNav message block (ID 5891)
type GpsNav struct {
	T          TimeStamp `json:"t"`
	Prn        uint8     `json:"prn"`
	_          uint8
	Wn         uint16 `json"wn"`
	L2Code     uint8  `json:"l2code"`
	Ura        uint8  `json:"ura"`
	Health     uint8  `json:"health"`
	L2Data     uint8  `json:"l2data"`
	Iodc       uint16 `json:"iodc"`
	Iode2      uint8  `json:"iode2"`
	Iode3      uint8  `json:"iode3"`
	FitIntFlag uint8  `json:"fit_int_flag"`
	_          uint8
	Tgd        float32 `json:t_gd"`
	Toc        uint32  `json:"t_oc"`
	Af2        float32 `json:"a_f2"`
	Af1        float32 `json:"a_f1"`
	Af0        float32 `json:"a_f0"`
	Crs        float32 `json:"c_rs"`
	DelN       float32 `json:"del_n"`
	M0         float64 `json:"m_0"`
	Cuc        float32 `json:"c_uc"`
	E          float64 `json:"e"`
	Cus        float32 `json:"c_us"`
	SqrtA      float64 `json:"sqrt_a"`
	Toe        uint32  `json:"t_oe"`
	Cic        float32 `json:"c_ic"`
	Omega0     float64 `json:"omega_0"`
	Cis        float32 `json:"c_is"`
	I0         float64 `json:"i_0"`
	Crc        float32 `json:"c_rc"`
	Omega      float64 `json:"omega"`
	OmegaDot   float32 `json:"omega_dot"`
	IDot       float32 `json:"i_dot"`
	WnToc      uint16  `json:"wn_t_oc"`
	WnToe      uint16  `json:wn_t_oe"`
}

// GPSAlm message block (ID 5892)
type GpsAlm struct {
	T        TimeStamp `json:"t"`
	Prn      uint8     `json:"prn"`
	_        uint8
	E        float32 `json:"e"`
	Ta       uint32  `json:"t_oa"`
	DeltaI   float32 `json:"delta_i"`
	OmegaDot float32 `json:"omega_dot"`
	SqrtA    float32 `json:"sqrt_a"`
	Omega0   float32 `json:"omega_0"`
	M0       float32 `json:"m_0"`
	Af1      float32 `json:"a_f1"`
	Af0      float32 `json:"a_f0"`
	WnA      uint8   `json:"wn_a"`
	Config   uint8   `json:"config"`
	Health8  uint8   `json:"health8"`
	Health6  uint8   `json:"health6"`
}

// GPSUtc message block (ID 5894)
type GpsUtc struct {
	T       TimeStamp `json:"t"`
	Prn     uint8     `json:"prn"`
	_       uint8
	A1      float32 `json:"a_1"`
	A0      float64 `json:"a_0"`
	Tot     uint32  `json:"t_ot"`
	Wnt     uint8   `json:"wn_t"`
	DelTLs  int8    `json:"del_t_ls"`
	WnLsf   uint8   `json:"wn_lsf"`
	Dn      uint8   `json:"dn"`
	DelTLsf int8    `json:"del_t_lsf"`
}

// GEONav message block (ID 5896)
type GeoNav struct {
	T    TimeStamp `json:"t"`
	Prn  uint8     `json:"prn"`
	_    uint8
	Iodn uint16  `json:"iodn"`
	Ura  uint16  `json:"ura"`
	T0   uint32  `json:"t0"`
	Xg   float64 `json:"xg"`
	Yg   float64 `json:"yg"`
	Zg   float64 `json:"zg"`
	Xgd  float64 `json:"xgd"`
	Ygd  float64 `json:"ygd"`
	Zgd  float64 `json:"zgd"`
	Xgdd float64 `json:"xgdd"`
	Ygdd float64 `json:"ygdd"`
	Zgdd float64 `json:"zgdd"`
}

// GLONav message block (ID 4004)
type GloNav struct {
	T      TimeStamp `json:"t"`
	Svid   uint8     `json:"svid"`
	FreqNr uint8     `json:"freqnr"`
	X      float64   `json:"x"`
	Y      float64   `json:"y"`
	Z      float64   `json:"z"`
	Dx     float32   `json:"dx"`
	Dy     float32   `json:"dy"`
	Dz     float32   `json:"dz"`
	Ddx    float32   `json:"ddx"`
	Ddy    float32   `json:"ddy"`
	Ddz    float32   `json:"ddz"`
	Gamma  float32   `json:"gamma"`
	Tau    float32   `json:"tau"`
	Dtau   float32   `json:"dtau"`
	Toe    uint32    `json:"t_oe"`
	WnToe  uint16    `json:"wn_toe"`
	P1     uint8     `json:"p1"`
	P2     uint8     `json:"p2"`
	E      uint8     `json:"e"`
	B      uint8     `json:"b"`
	Tb     uint16    `json:"tb"`
	M      uint8     `json:"m"`
	P      uint8     `json:"p"`
	L      uint8     `json:"l"`
	P4     uint8     `json:"p4"`
	Nt     uint16    `json:"n_t"`
	Ft     uint16    `json:"f_t"`
}

// GLOAlm message block (ID 4005)
type GloAlm struct {
	T       TimeStamp `json:"t"`
	Svid    uint8     `json:"svid"`
	FreqNr  uint8     `json:"freqnr"`
	Epsilon float32   `json:"epsilon"`
	Toa     uint32    `json:"t_oa"`
	DeltaI  float32   `json:"delta_i"`
	Lambda  float32   `json:"lambda"`
	Tln     float32   `json:"t_ln"`
	Omega   float32   `json:"omega"`
	DeltaT  float32   `json:"delta_t"`
	DdeltaT float32   `json:"d_delta_t"`
	Tau     float32   `json:"tau"`
	WnA     uint8     `json:"wn_a"`
	C       uint8     `json:"c"`
	N       uint16    `json:"n"`
	M       uint8     `json:"m"`
	N4      uint8     `json:"n_4"`
}
