package sbf

import "testing"

func TestCrc(t *testing.T) {
	table := []struct {
		msg  []byte
		csum uint16
	}{
		{msg: []byte("\x00\x05\x01"), csum: 0xefd4},
	}

	for _, e := range table {
		crc := Crc16(e.msg, 0)
		if crc != e.csum {
			t.Errorf("CRC error (%q); expected %x, got %x", e.msg, e.csum, crc)
		}
	}
}
