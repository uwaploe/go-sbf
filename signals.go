package sbf

// Signal number codes
const (
	SigGpsL1CA int = iota
	SigGpsL1P
	SigGpsL2P
	SigGpsL2C
	SigGpsL5
	// Code 5 reserved
	_
	SigQzssL1CA
	SigQzssL2C
	SigGlonassL1CA
	SigGlonassL1P
	SigGlonassL2P
	SigGlonassL2CA
	SigGlonassL3
	// Codes 13, 14 reserved
	_
	_
	SigIrnssL5
	// Code 16 reserved
	_
	SigGalileoE1
	// Code 18 reserved
	_
	SigGalileoE6
	SigGalileoE5a
	SigGalileoE5b
	SigGalileoE5AltBOC
	SigMssLBand
	SigSbasL1CA
	SigSbasL5
	// Code 27 reserved
	_
	SigBeiDouB1
	SigBeiDouB2
	SigBeiDouB3
	// Code 31 reserved
	_
)

// Signal type codes
const (
	L1CA int = iota + 1
	L1P
	L2P
	L2C
	L5
	L2CA
	L3
	L1BC
	E6BC
	E5A
	E5B
	E5ALTBOC
	LBAND
	B1
	B2
	B3
)

type signalType struct {
	code  int
	sys   string
	f     float64
	rinex string
}

const (
	MHZ    float64 = 1e6
	CLIGHT float64 = 299792458.0
)

// Signal types indexed by the Signal Number Code.
var signalTable = [32]signalType{
	{code: L1CA, sys: "GPS", f: 1575.42 * MHZ, rinex: "1C"},
	{code: L1P, sys: "GPS", f: 1575.42 * MHZ, rinex: "1W"},
	{code: L2P, sys: "GPS", f: 1227.60 * MHZ, rinex: "2W"},
	{code: L2C, sys: "GPS", f: 1227.60 * MHZ, rinex: "2L"},
	{code: L5, sys: "GPS", f: 1176.45 * MHZ, rinex: "5Q"},
	{},
	{code: L1CA, sys: "QZSS", f: 1575.42 * MHZ, rinex: "1C"},
	{code: L2C, sys: "QZSS", f: 1227.60 * MHZ, rinex: "2L"},
	{code: L1CA, sys: "GLONASS", f: 1602.00 * MHZ, rinex: "1C"},
	{code: L1P, sys: "GLONASS", f: 1602.00 * MHZ, rinex: "1P"},
	{code: L2P, sys: "GLONASS", f: 1246.00 * MHZ, rinex: "2P"},
	{code: L2CA, sys: "GLONASS", f: 1246.00 * MHZ, rinex: "2C"},
	{code: L3, sys: "GLONASS", f: 1202.025 * MHZ, rinex: "3Q"},
	{},
	{},
	{code: L5, sys: "IRNSS", f: 1176.45 * MHZ, rinex: "5A"},
	{},
	{code: L1BC, sys: "Galileo", f: 1575.42 * MHZ, rinex: "1C"},
	{},
	{code: E6BC, sys: "Galileo", f: 1278.75 * MHZ, rinex: "6C"},
	{code: E5A, sys: "Galileo", f: 1176.45 * MHZ, rinex: "5Q"},
	{code: E5B, sys: "Galileo", f: 1207.14 * MHZ, rinex: "7Q"},
	{code: E5ALTBOC, sys: "Galileo", f: 1191.795 * MHZ, rinex: "8Q"},
	{code: LBAND, sys: "MSS"},
	{code: L1CA, sys: "SBAS", f: 1575.42 * MHZ, rinex: "1C"},
	{code: L5, sys: "SBAS", f: 1176.45 * MHZ, rinex: "5I"},
	{code: L5, sys: "QZSS", f: 1176.45 * MHZ, rinex: "5Q"},
	{},
	{code: B1, sys: "BeiDou", f: 1561.098 * MHZ, rinex: "2I"},
	{code: B2, sys: "BeiDou", f: 1207.14 * MHZ, rinex: "7I"},
	{code: B3, sys: "BeiDou", f: 1268.52 * MHZ, rinex: "6I"},
	{},
}
