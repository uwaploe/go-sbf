package sbf

import "testing"

func TestRinexId(t *testing.T) {
	table := []struct {
		svid  int
		rinex string
	}{
		{svid: 0, rinex: ""},
		{svid: 9, rinex: "G09"},
		{svid: 37, rinex: "G37"},
		{svid: 47, rinex: "R10"},
		{svid: 62, rinex: ""},
		{svid: 65, rinex: "R27"},
		{svid: 120, rinex: "S20"},
		{svid: 200, rinex: "S43"},
	}

	for _, e := range table {
		if RinexId(e.svid) != e.rinex {
			t.Errorf("Bad ID; expected %q, got %q", e.rinex, RinexId(e.svid))
		}
	}
}

func TestNewBlock(t *testing.T) {
	raw := []byte("$@MT\xa7O`\x00\xc0\x7f\x11\x18\x1a\t\x06\x00\xb2\b[\xa3\x9d\x9d\xea?\x9d8\xfa\x91#\x14\x01\xc04>&\xbf\x02\xfb6@\xb4Y\x8a\xc1\xecD|:q\xe4\x7f\xba2P\x1f\xbb\xf9\x02\x95\xd0\xc8\xfcL= \xcf\xa1\xbe\xba\xb0\xb59\x00\x00\x10\a\x83\x00\xbd\x00\x01\x01\x00\x00\x01\x01\x00\x006\x00\xac\x00\xd4\x00 \x00")
	b, err := NewBlock(raw, false)
	if err != nil {
		t.Fatal(err)
	}

	if id := b.BlockNum(); id != 4007 {
		t.Errorf("Bad block number; expected 4007, got %d", id)
	}
}
